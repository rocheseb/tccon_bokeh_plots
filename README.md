### README ###

This app requires python 2.7.x (not tested with python 3.x) and bokeh (tested up to 0.12.16) and netCDF4 installed.

	https://bokeh.pydata.org/en/latest/docs/installation.html

Public TCCON files can be downloaded from http://tccon.ornl.gov/

Assuming you have Anaconda you can create a python environment that can run this code using the envrionment.yml file:

	conda env create -f environment.yml

## How to use this app:

	- Put TCCON .eof.csv or .nc files in the 'data' folder of this app, only one type of file should be in the 'data' folder

	- Run the app with the command "bokeh serve --show tccon_app"

	- While the server is running, the app will be available in the browser at localhost:5006/tccon_app

## init.py ##

For private TCCON files there are 1200+ variables. I only make a subset of those available for selection in the variable dropdown widgets.
You can access more variables by editing the 'skip_list' list in the init.py program

This code can read from any .eof.csv files.

You just need to add the appropriate key:value pairs to the T_FULL and T_LOC dictionaries in init.py

## Server setup

Add to nginx conf at /etc/nginx/nginx.conf

location /plots {
                proxy_pass http://127.0.0.1:5006;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection "upgrade";
                proxy_http_version 1.1;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header Host $host:$server_port;
                proxy_buffering off;
        }

Restart web server with 'sudo service nginx restart'

Add to /etc/systemd/system/plot.service

[Unit]
Description=Bokeh Plot
After=syslog.ddtarget network.target remote-fs.target nss-lookup.target

[Service]
Type=simple
WorkingDirectory=/var/www/tccon-plotting/
ExecStart=/opt/anaconda/anaconda2/bin/bokeh serve --allow-websocket-origin tccondata.org --prefix plots plots
Restart=on-failure

[Install]
WantedBy=multi-user.target


Reload by typing 'sudo systemctl daemon-reload'

You can start up bokeh with 'sudo systemctl start plot'

Start the plotting server on reboot with 'sudo systemctl enable plot'

# cache #

In the init.py there is a "cache_max_size" variable set to 0 by default

It is only useful when reading fron .eof.csv files in order to load recently loaded variables faster. It is not useful when reading from netcdf files.

## Contact ##

sebastien.roche@mail.utoronto.ca
